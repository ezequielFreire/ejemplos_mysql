--#######################################################
--#### Formas de recuperar información de una tabla #####
--#######################################################

--#Utilización del comando select
--En esta consulta lo que hacemos es decirle a la base que traiga el apellido
--de la tabla alumno donde el nombre del alumno sea Estefania
select apellido from alumnos
	where nombre = 'Estefania';

--Realizamos una consulta similar a la anterior pero en este caso pedimos dos campos
--de la base, que son el apellido y el ID del alumno, siempre y cuando se llame 
--Estefania
select apellido,id_alumnos from alumnos
	where nombre = 'Estefania';

--Al ejecutar esta consulta lo que hacemos es traer todos los campos de los estudiantes
--que se llamen Estefania, para ello usamos el comodin (*)
select * from alumnos
	where nombre = 'Estefania';

--Realizando esta consulta traemos todos los campos de los primeros cuatro alumnos
select * from alumnos
	where id_alumno < 5;

--Relizando esta consulta traemos la información de los alumnos con id mayores a 5
-- o que su nombre sea Jose y el apellido Martinez
--Nota: La prioridad del orden en que se coloque OR o AND es fundamental en la consulta
-- ya que determinara que el diseño de la consulta sea exitoso o no.
--Nota: el operador AND tiene prioridad sobre el operador OR.
select * from alumnos
	where  id_alumnos > 5 
		or
		   nombre = 'Jose' and apellido = 'Martinez';


--######################################################
--####### Correspondencia de patrones ##################
--######################################################
--Comodines para hacer consultas sin conocer el nombre completo de lo que se busca
--Ejemplo queremos traer todos los campos del alumno Marina Muñoz, pero no nos
--acordamos si el apellido termina con s o con z, Muños o Muños
select * from alumnos
	where apellido like 'Muño%';


--Tambien podemos realizar una consulta de apellidos que tengan la letra e
select * from alumnos
	where apellido like '%e%';

--AHora si queremos realizar una consulta que nos traiga todos los campos de los
-- de los apellidos de los alumnos que comiencen con la letra p
select * from alumnos
	where apellido like 'p%';

--######################################################
--####### Ordenas datos de una tabla ###################
--######################################################
--Ordenar datos por nombre
select * from alumnos
	order by nombre;

--Ordenamos la tabla tambien por nombre y apellido
select * from alumnos
	order by nombre, apellido;
 
--Ordenamos la tabla de forma descendente
select * from alumnos
	order by id_alumnos desc;

--Ordenamos la tabla de forma ascendente
select * from alumnos
	order by id_alumnos asc;

--Ordenamos la tabala con diferentes ocndiciones
select * from alumnos
	order by id_alumno asc, nombre desc, apellido asc;

--#####################################################
--####### Limitar la cantidad de resultados ###########
--#####################################################
select * from alumnos
   order by id_alumno desc limit 1;
--+-----------+--------+----------+------+------------+------------------+
--| id_alumno | nombre | apellido | sexo | telefono   | fecha_nacimiento |
--+-----------+--------+----------+------+------------+------------------+
--|        16 | Julian | Peralta  | m    | 1545779976 | 1988-05-05       |
--+-----------+--------+----------+------+------------+------------------+
--1 row in set (0.00 sec)


--Nota: limit 0 no devuelve registros, pero es una buena forma de probar una
--consulta en una base de datos de gran tamaño sin ejecutarla.

--Podemos utilizar la un desplazamiento del resultado por ejemplo en la sigueinte
--consulta, del resultado obtenido se desplaza uno asi que trae el segundo registro
--Nota: Si se incluyen dos numeros el primero es el desplazamiento y el segundo la
--cantidad de columnas
select * from alumnos
	order by id_alumno desc limit 1,1;

--+-----------+--------+----------+------+------------+------------------+
--| id_alumno | nombre | apellido | sexo | telefono   | fecha_nacimiento |
--+-----------+--------+----------+------+------------+------------------+
--|        15 | Juan   | Peralta  | m    | 1546779876 | 1982-01-05       |
--+-----------+--------+----------+------+------------+------------------+

--##########################################################
--####### Devolver valores con la funcion MAX y MIN ########
--##########################################################

select max(id_alumno) from alumnos; 

--+----------------+
--| max(id_alumno) |
--+----------------+
--|             16 |
--+----------------+

select min(id_alumno) from alumnos;

--###########################################################
--##### Recuperar registros que no sean iguales #############
--###########################################################
--Si no deseamos recuperar datos duplicados tenemos una instrucción que es de
--utilidad en estos casos que es DISTINCT que traerá resultados que no se iguales 
--en la consulta

select distinct apellido from alumnos
	order by apellido

--+-----------+
--| apellido  |
--+-----------+
--| Diaz      |
--| Figueredo |
--| Garay     |
--| Garcia    |
--| Gomez     |
--| Martinez  |
--| Minerva   |
--| Muñoz     |
--| Peralta   |
--| Perez     |
--| Villa     |
--+-----------+
--11 rows in set (0.03 sec)

--############################################################
--####### Como contar la cantidad de registros ############### 
--############################################################

select count(apellidos) from alumnos;

--+-----------------+
--| count(apellido) |
--+-----------------+
--|              16 |
--+-----------------+
--1 row in set (0.02 sec)

--Usamos count(*) para contar la cantidad total de registros
select count(*) from alumnos;

--+----------+
--| count(*) |
--+----------+
--|       16 |
--+----------+
--1 row in set (0.00 sec)

--Con la siguiente consulta contamos lo cantidad de apellidos que no se repitan
select count(distinct apellido) from alumnos;

--+--------------------------+
--| count(distinct apellido) |
--+--------------------------+
--|                       11 |
--+--------------------------+
--1 row in set (0.02 sec)

--############################################################
--##### Como recuperar la media AVG y el total SUM ###########
--############################################################
--En este caso como las tablas no tienen nota usaremos los id_alumnos
--para el fin de ejemplo nos servirá

--Sacamos el promedio de los id, es como sumar todos los id y dividirlos por la
--cantidad de id que tengamos
select avg(id_alumno) from alumnos;

--+----------------+
--| avg(id_alumno) |
--+----------------+
--|         8.5000 |
--+----------------+
--1 row in set (0.01 sec)

--sumamos la cantidad de id que tenemos id1+id2+...+idn
select sum(id_alumno) from alumnos;

--+----------------+
--| sum(id_alumno) |
--+----------------+
--|            136 |
--+----------------+
--1 row in set (0.00 sec)

--#######################################################
--###### Realizacion de calculos ########################
--#######################################################

select 1 + 1;

--+-------+
--| 1 + 1 |
--+-------+
--|     2 |
--+-------+
--1 row in set (0.00 sec)

--Realizar cálculos resulta útil, por ejemplo si se necesita saber como se vería
--un registro si se incrementara eje. un pago con el iva
select id_alumno + 1 from alumnos;

--#######################################################
--###### Eliminación de registros DELETE ################
--#######################################################

--Para la eliminación de un dato se necesita un campo exclusivo 
--Nota: Debemos tener cuidado al momento de utilizar esta instruccion 
--ya que si no especificamos una condicion podemos borrar todos los 
--registros ej. delete from alumnos;

--Al realizar la siguiente consulta borramos el registro siempre y cuando el 
-- id sea igual a 16
delete from alumnos
	where id_alumno = 16;

--###########################################################
--###### Como cambiar los registros de una tabla con UPDATE #
--###########################################################

--De igual manera que con delete necesitamos usar where para limitar ya que
--podriamos actualizar todos los campos
--con la siguiente consulta lo que haremos sera es modificar el apellido de
--Jesica Garcia, por el apellido correcto que es Garmendia
update alumnos set apellido = 'Garmendia'
	where id_alumno = 10;

--###########################################################
--##### Eliminacion de tablas y base de datos ###############
--###########################################################
--Nota: Devemos tener cuidado, ya que al utilizar la instruccion DROP borramos 
--la base o la tabla seleccionada con todos sus datos

--Crearesmos una tabla y la eliminaremos y luego haremos lo mismo con una base
create table comision(id int);
drop table comision;

create schema casa;
drop schema casa;

--############################################################
--######## Como agregar una columna ALTER ####################
--############################################################
--Agregamos una tablaque se llama nota
alter table alumnos add nota_uno int(10);

--Agregamos una tabla que se llame año_nacimiento
alter table alumnos add año_nacimiento year;

--Forma de cambiar el nombre del campo change
alter table alumnos change año_nacimiento anio_ingreso date;

--Para cambiar el tipor de dato de un campo sin cambiar el nombre como con change
-- utilizamos MODIFY
alter table alumnos modify nota int;

--############################################################
--######### Cambiar el nombre de una tabla ###################
--############################################################
--creamos una tabla nueva y luego modificaremos el nombre
create table instructores (id_profesor int);
alter table instructores rename profesores;

--Otra forma de hacer lo mismo que la instruccion anterior es rename to
alter table profesores rename to instructores;


--###########################################################
--###### Como eliminar una columna de la tabla ##############
--###########################################################

alter table alumnos drop anio_ingreso;

--###########################################################
--###### utilizaremos update para agregar las notas #########
--###########################################################

update alumnos set nota_uno = 10 where id_alumno = 1;
update alumnos set nota_uno = 5 where id_alumno = 2;
update alumnos set nota_uno = 5 where id_alumno = 3;
update alumnos set nota_uno = 7 where id_alumno = 4;
update alumnos set nota_uno = 10 where id_alumno = 5;
update alumnos set nota_uno = 9 where id_alumno = 6;
update alumnos set nota_uno = 8 where id_alumno = 7;
update alumnos set nota_uno = 2 where id_alumno = 8;
update alumnos set nota_uno = 1 where id_alumno = 9;
update alumnos set nota_uno = 6 where id_alumno = 10;    		
update alumnos set nota_uno = 7 where id_alumno = 11;
update alumnos set nota_uno = 4 where id_alumno = 12;
update alumnos set nota_uno = 5 where id_alumno = 13;
update alumnos set nota_uno = 7 where id_alumno = 14;
update alumnos set nota_uno = 9 where id_alumno = 15;

--Si queremos saber si se agregaron correctamente
select nota_uno from alumnos;


--########################################################
--########## Como especificar el formato de fecha ########
--########################################################

--Fecha con diferentes formatos

select date_format(fecha_nacimiento,'%d/%m/%Y') from alumnos;

select date_format(fecha_nacimiento,'%W %M %e %Y') from alumnos;

select date_format(fecha_nacimiento,'%a %D %b, %Y') from alumnos;

--Hora actual y fecha actual
select now(), current_date();

--+---------------------+----------------+
--| now()               | current_date() |
--+---------------------+----------------+
--| 2015-02-07 23:11:08 | 2015-02-07     |
--+---------------------+----------------+

--la funcion year solo tomara el año de la fecha de nacimiento
select year(fecha_nacimiento) from alumnos;

-- la funcion month
select month(fecha_nacimiento),dayofmonth(fecha_nacimiento)from alumnos;

--##############################################################
--#####Consultas avanzadas #####################################
--##############################################################

--Uso de alias para combiar el nombre de una tabla durante la consulta
select nombre,apellido,month(fecha_nacimiento)
       as mes,day(fecha_nacimiento)as dia from alumnos;

--+-----------+-----------+------+------+
--| nombre    | apellido  | mes  | dia  |
--+-----------+-----------+------+------+
--| Ricardo   | Perez     |    5 |    1 |
--| Jose      | Martinez  |    3 |   15 |
--| Julian    | Villa     |    9 |    6 |
--| Javier    | Gomez     |    8 |   11 |
--| Martin    | Garay     |    5 |   24 |
--| Marina    | Muñoz     |    2 |   12 |
--| Estefania | Perez     |   12 |   28 |
--| Monica    | Minerva   |    3 |    9 |
--| Natalia   | Martinez  |    1 |   11 |
--| Jesica    | Garmendia |    2 |    4 |
--| Juliana   | Diaz      |    6 |    9 |
--| Pedro     | Peralta   |    4 |   15 |
--| Micaela   | Garcia    |    2 |   24 |
--| Vanina    | Figueredo |    4 |   12 |
--| Juan      | Peralta   |    1 |    5 |
--+-----------+-----------+------+------+

-- Concatenar valores de las tablas
select concat(nombre,' ',apellido) from alumnos;

--+-----------------------------+
--| concat(nombre,' ',apellido) |
--+-----------------------------+
--| Ricardo Perez               |
--| Jose Martinez               |
--| Julian Villa                |
--| Javier Gomez                |
--| Martin Garay                |
--| Marina Muñoz                |
--| Estefania Perez             |
--| Monica Minerva              |
--| Natalia Martinez            |
--| Jesica Garmendia            |
--| Juliana Diaz                |
--| Pedro Peralta               |
--| Micaela Garcia              |
--| Vanina Figueredo            |
--| Juan Peralta                |
--+-----------------------------+
--15 rows in set (0.02 sec)

--Buscar el dia del año de una fecha, dias del 1 al 366
select dayofyear(fecha_nacimiento) from alumnos
	where id_alumno = 1;

--+-----------------------------+
--| dayofyear(fecha_nacimiento) |
--+-----------------------------+
--|                         121 |
--+-----------------------------+
--1 row in set (0.06 sec)

