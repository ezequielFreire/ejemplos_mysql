--############################################################
--######## Tipos de datoy y tablas       #####################
--############################################################

--Mysql utiliza varios tipos de dato y de tablas, el tipo de tabla que se encuentra
--predeterminada es MyISAM que esta optimizada para la velocidad del comando SELECT
--en la mayoria de las bases de datos se utiliza este tipo de tablas ya que utilizan
--mas SELECT que las INSTRUCCIONES INSERT y UPDATE.

--Los tipos numericos soportan dos tipos: UNSIGNED y ZEROFILL
--UNSIGNED: Solo permite numeros positivos y extinede el numero de enteros
--ZEROFILL: rellena el lugar con zero del lado izquierdo 003


create table test1(
	id tinyint zerofill
);

insert into test1 values(3);

insert into test1 values(-1);

insert into test1 values(256);

select * from test1;
--+------+
--| id   |
--+------+
--|  003 |
--|  000 |
--|  255 |
--+------+

--#####################################################################
--###### Tipos de valores numericos ###################################
--#####################################################################

--tinyint  -128 a 127
--bit      sinonimo de tinyint
--bool     sinonimo de tinyint
--smallint 32.768 a 32.767
--mediumint 8.388.608 a 8.388.607 
--int -2.147.483.648 a 2.147.483.647
--integer sinonimo de int
--bigint -9.223.372.036.854.775.808 a 9.223.372.036.854.775.807
--float  numero con coma
--double numero con coma mayor precicion 
--real  sinonimo de doble
--decimal un numero almacenado como una cadena
--dec sinonimo de decimal
--numeric otro sinonimo de decimal

--Nota : si se asigna un valor que supera el numero asignado a la base

 create table test2 (
	id tinyint(10)
);

--Inserto un valor que supera lo asignado en la base que es 10
insert into test2(id) values(100000000000);
--ERROR: 
--No query specified

--Query OK, 1 row affected, 1 warning (0.07 sec)


--verificamos que es lo que se inserto en la base
select * from test2;

--+------+
--| id   |
--+------+
--|  127 |
--+------+
--1 row in set (0.00 sec)


--Si intentamos restringir un tipo a un limite inferior al permitido el valor no se 
--recortara

create table test3(
	in int(1);
);

insert into test3(id) values(45434622);

select * from test3;

--+----------+
--| id       |
--+----------+
--| 45434622 |
--+----------+
--1 row in set (0.00 sec)

--Zerofill se suele utilizar por es mas sencillo ver los asi los resultados

create table test4(
	id int(3) zerofill,
	id2 int zerofill
);

insert into test4(id,id2) values(22,22);

select * from test4;

--+------+------------+
--| id   | id2        |
--+------+------------+
--|  022 | 0000000022 |
--+------+------------+
--1 row in set (0.00 sec)

--Nota: El efecto de la especificacion del ancho en id limita a tres 10s caracteres
--que se representan, aunque el campo i d 2 utilice un tipo I N T predeterminado (1 0) --sin firmar.


--####################################################################
--######## Tipos de culmnas de cadenas ###############################
--####################################################################

--char-- de 0 a 255 caracteres, longitud fija ,rellena los espacios
--varchar-- De 0 a 255 caracteres. longitud variable, elimina los espacios al guardar
--tinyblob de 255 (2& - 1),se aconseja usar varchar binary discrimina entre 
--mayusculas y minusculas
--tinytext-- 255 (281), se aconseja usar varchar no discrimina entre mayusculas y 
--minusculas
--blob--objeto binario grande discrimina entre mayusculas y minusculas,
--Maximo de 65.535 caracteres (216 1)
--text-- Maximo de 65.535 caracteres (216 1), no discrimina entre 
--mayusculas y minusculas
--mediumblob-- Objeto binario grande de tarnaiio medio. Maximo de 16.777.215 
--caracteres (224 - I ).no discrimina entre rnayusculas y minusculas.
--mediumtext--Maximo de 16.777.21 5 caracteres (224 - 1).no discrimina entre --
--mayusculas y minusculas.
--longblob-- Objeto binario grande de gran tarnaiio. Maximo de
--4.294.967.295 caracteres ( P 2 1),discrimina entre mayusculas y minusculas.
--longtext--Maximo de 4.294.967.295 caracteres ( P 2 - 1).no discrimina entre
--mayusculas y minusculas.
--enum('valorl','valor2', ...) Valores maximos de 65.535
--set('valorl', 'valor2', ...) Puede contener d e cero a 64 valores de la lista
--especificada.


--##########################################################################
--####### Cosas para tener encuenta al almacenar texto #####################
--##########################################################################

--Para lograr mayor velocidad, utilice columnas fijas, como CHAR.
--Para ahorrar espacio, utilice columnas dinamicas, como VARCHAR.
--Para limitar 10s contenidos de una columna a una opcion, utilice ENUM.
--Para permitir mas de una entrada en una columna, seleccione S E T
--Si desea buscar testo sin discriminar entre mayusculas y minusculas, utilice TEXT.
--Si desea buscar testo discriminando entre mayusculas y minusculas, utilice BLOB.
--Para imagenes y otros objetos binarios, almacenelos en el sistema de archivos
--en lugar de directamente en la base de datos.


