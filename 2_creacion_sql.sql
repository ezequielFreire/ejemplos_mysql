--###################################################
--###### creacion de una base de datos o esquema ####
--###################################################
-- #Primero creamos un esquema, el cual es una base para un proyecto
create schema facultad; --MySQL
-- create database facultad; --SQL

-- #Luego seleccionamos el esquema
use facultad

--###################################################
--####### Creacion de tablas dentro de la base ######
--###################################################
-- #Luego creamos una tabla alumno que se ira modificando mientras agreguemos
-- #nuevas tablas relacionadas 

create table alumnos(
    id_alumno int unsigned not null auto_increment
   ,nombre varchar(30) not null default ''
   ,apellido varchar(30) not null default ''
   ,sexo enum('m','f') not null default 'm'
   ,telefono varchar(30) not null default ''
   ,fecha_nacimiento date not null default '0000-00-00'
   ,primary key (id_alumno)		
);

--###################################################
--####### Insertar datos en una tabla ###############
--###################################################
-- #Insertamos alumnos a la tabla de forma simple

insert into alumnos values(1,'Ricardo','Perez','m',1567884590,'1987-5-1');
insert into alumnos values(2,'Jose','Martinez','m',1560909976,'1989-3-15');
insert into alumnos values(3,'Julian','Villa','m',1530509776,'1988-9-6');
insert into alumnos values(4,'Javier','Gomez','m',1590879877,'1995-8-11');
insert into alumnos values(5,'Martin','Garay','m',1570909874,'1996-5-24');
insert into alumnos values(6,'Marina','Muñoz','f',1550869377,'1990-2-12');
insert into alumnos values(7,'Estefania','Perez','f',1582809679,'1991-12-28');
insert into alumnos values(8,'Monica','Minerva','f',1581859871,'1986-3-9');
insert into alumnos values(9,'Natalia','Martinez','f',1580849975,'1985-1-11');
insert into alumnos values(10,'Jesica','Garcia','f',1582889876,'1992-2-4');
insert into alumnos values(11,'Juliana','Diaz','f',1580809876,'1993-6-9');
insert into alumnos values(12,'Pedro','Peralta','m',1583879876,'1982-4-15');
insert into alumnos values(13,'Micaela','Garcia','f',1585589877,'1992-2-24');
insert into alumnos values(14,'Vanina','Figueredo','f',1511809886,'1997-4-12');
insert into alumnos values(15,'Juan','Peralta','m',1546779876,'1982-1-5');

--#Forma completa de utilizar insert, es recomendable si utilizamos consultas 
--desde algun lenguaje de programacion ya que reducimos la posibilidad de error
--tambien es mas facil para la base ingresar los datos separados por como.
insert into alumnos (id_alumno,nombre,apellido,sexo,telefono,fecha_nacimiento,)
	values (16,'Marcelo','Villalba','m',1583879876,'1992-3-12');

--#Insercion de grandes cantidades de datos desde un archivo de texto 
-- con load data
load data local infile "alumnos.sql" into table alumnos;

--forgmato del archivo alumno.sql
-- 1\tRicardo\tPerez\tm\t1567884590\t1987-5-1
-- 2\tJose\tMartinez\tm\t1560909976\t1989-3-15


-- #Borrar una tabla
-- drop table alumnos

-- #Borramos un alumno
delete from alumnos
	where id_alumno = 15;

-- #Hacemos una consulta que traiga a todos los alumnos femeninos 'f'
select * from alumnos
	where sexo = 'f'


