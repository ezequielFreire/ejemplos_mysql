--################################################################
--##### Tipos de tablas ISAM, MyISAM, MARGE,InnoDB #####################
--################################################################


create table sales_rep1(
	id int auto_increment primary key
	,employee_number int(11)
	,surname varchar(40)
	,firs_name varchar(30)
	,commission tinyint(4)
	,date_joined date
    ,birthday date
) engine=MyISAM;
--type=myisam; cambia por engine en versiones superiores a las 3 de mysql

create table sales_rep2(
	id int auto_increment primary key
	,employee_number int(11)
	,surname varchar(30)
	,firs_name varchar(30)
	,commission tinyint(4)
	,date_joined date
	,birthday date
)engine=myisam;

--Las tablas MERGE fusionan las dos tablas MyISAM iguales
create table sales_rep1_2(
	id int auto_increment primary key
	,employee_number int(11)
	,surname varchar(30)
	,firs_name varchar(30)
	,commission tinyint(4)
	,date_joined date
	,birthday date
)engine=merge
union=(sales_rep1,sales_rep2);

--Insertamos datos en sales_rep1 y sales_rep2
insert into sales_rep1
	('employee_number','surname','firs_name','commission','date_joined','birthday')
	values
	(1, 'Tshwete' , 'Paul' ,1, '1999-01-03', '1970-03-04');

insert into sales_rep1
	('employee_number','surname','firs_name','commission','date_joined','birthday')
	values
	(0,2,'Gr0b1er1','Peggy-Sue',2 ,'2001-11-19','1956-08-2');
	

