--###############################################
--##### Despues de realizar la instalacion ######
--###############################################

--#Comandos basicos de MySQL

--Ingresar a MySQL desde la terminal
mysql -u root -r

--Cambiar o setear un password al usuario
set password = password ('mi_password');

--Crear una base o esquema
create database firstdb;

--Creacion de usaurio con el que se trabajara en la base de datos y con los
--persmisos completos
grant all on firstdb.* to userbase@localhost
indentified by 'mi_password';

--Salir de la base de datos 
exit;

--Realizar un listado de tablas
show tables;

--Anlalizar la estructura de tablas con describe
describe alumnos;




